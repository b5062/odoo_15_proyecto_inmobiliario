{
    "name":"Seguimiento de inmuebles v2.0",
    "version":"2.0",
    "author":"BigOdoo <hola@bigodoo.com>",
    "website":"http://www.bigodoo.com",
    "depends":["base"],
    "demo":[
        "demo/inmuebles.xml"
    ],
    "data":[
        "security/groups.xml",
        "security/rules.xml",
        "security/ir.model.access.csv",
        "demo/inmuebles.xml",
        "views/views.xml"
    ]
}