from locale import currency
from odoo import models,fields,api



class BODisponibilidad(models.Model):
    _name = "bo.disponibilidad"
    _description = "Disponibilidad inmuebles"

    name = fields.Char(string="Disponiblidad")
    company_id = fields.Many2one("res.company",string="Compañia",default=lambda self:self.env.company.id)


class BOTags(models.Model):
    _name = "bo.tag"
    _description = "Etiquetas de inmuebles"

    name = fields.Char(string="Etiqueta")
    company_id = fields.Many2one("res.company",string="Compañia",default=lambda self:self.env.company.id)

class BOInmueble(models.Model):
    _name = "bo.inmueble"
    _description = "Inmuebles"

    name = fields.Char(string="Inmueble")
    value = fields.Float(string="Precio USD")
    tag_ids = fields.Many2many("bo.tag",string="Etiquetas")

    street = fields.Char("Dirección")
    street_2 = fields.Char("Dirección 2")
    currency_id = fields.Many2one("res.currency",string="Moneda")
    image = fields.Binary("Imagen")

    type = fields.Selection(selection=[("casa","Casa"),
                                        ("departamento","Departamento"),
                                        ("terreno","Terreno")],
                                        string="Tipo de inmueble",
                                        required=True)

    user_id = fields.Many2one("res.users",
                             string="Responsable",
                             default=lambda self:self.env.user.id)
    
    company_id = fields.Many2one("res.company",string="Compañia",default=lambda self:self.env.company.id)

    @api.model
    def _expand_disponibilidad(self,states,domain,order):
        return self.env["bo.disponibilidad"].search([('company_id','=',self.env.company.id)])

    disponibilidad_id = fields.Many2one("bo.disponibilidad",string="Disponibilidad de inmueble",group_expand='_expand_disponibilidad')

    def _lista_disponibilidad(self):
        result = self.env["bo.disponibilidad"].search([('company_id','=',self.env.company.id)])
        return result.mapped(lambda r:(r.name,r.name))
        """
            [('En construcción','En construccion'),('Vendido','Vendido')]
        """

    disponibilidad = fields.Selection(selection=_lista_disponibilidad,string="Disponibilidad")

    def detalle_inmueble(self):
        return {
            "type":"ir.actions.act_window",
            "res_model":"bo.inmueble",
            "res_id":self.id,
            "view_mode":"form"
        }
