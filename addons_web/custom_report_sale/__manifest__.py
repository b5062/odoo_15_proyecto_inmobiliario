{
    "name":"Reportes de venta",
    "depends":[
        "base_setup",
        "sale"
    ],
    "data":[
        "template_sale.xml"
    ],
    "assets":{
        "web.report_assets_common":[
            "custom_report_sale/static/src/scss/*.scss"
        ]
    }
}