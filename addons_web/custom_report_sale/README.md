select so.name,
		so.partner_id,
		so.date_order,
		so.state,
		so.user_id,
		so.team_id,
		sol.product_id,
		sol.name,
		sol.price_unit,
		sol.price_subtotal,
		pt.categ_id 
	from sale_order so
	left join sale_order_line sol on sol.order_id = so.id
	left join product_product pp on pp.id = sol.product_id 
	left join product_template pt on pt.id = pp.product_tmpl_id 
	