from odoo import models,api,fields
from odoo.exceptions import UserError

class BOLibroReclamaciones(models.Model):
    _inherit = "bo.libro.reclamaciones"

    image_product = fields.Binary("Imagen de producto")

    attachment_ids = fields.Many2many("ir.attachment",string="Archivos adjuntos")

    partner_ids = fields.Many2many("res.partner")

    

    @api.constrains('partner_ids')
    def _constrains_partner_ids(self):
        if len(self.partner_ids.filtered(lambda p:p.company_type == 'company')) < 2:
            raise UserError("Recuede que debe tener por lo menos dos reclamantes de tipo compañía")



    



    # Many2one : Solo necesita el nombre de modelo
    # Many2many:  Solo necesita el nombre de modelo
    # One2many : Necesita el nombre del modelo y el id foraneo de este modelo que hace referencia al actual modelo