{
    "name":"Personalización de Libro de reclamaciones para Grupo Coinp",
    "depends":[
        "base","base_setup","web","bo_libro_reclamaciones"
    ],
    "data":[
        "data/paperformat.xml",
        "views/res_company.xml",
        "views/bo_libro_reclamaciones.xml",
        "report/report_layout_coinp.xml",
        "report/report_libro_reclamaciones.xml",
        "data/mail_template.xml"
    ],
    "assets":{
        "web.report_assets_common":[
            # "custom_coinp_libro_reclamaciones/static/src/scss/report.scss",
            # "custom_coinp_libro_reclamaciones/static/src/scss/sale_order.scss"
            "custom_coinp_libro_reclamaciones/static/src/*/*.scss"
        ]
    }
}