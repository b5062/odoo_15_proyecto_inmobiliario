from odoo import http,_
from odoo.http import request
import logging
_logger = logging.getLogger(__name__)

class MainLibroReclamaciones(http.Controller):

    def obtener_reclamos_objs(self):
        reclamaciones = request.env["bo.libro.reclamaciones"].search([])
        # _logger.info(reclamaciones)
        return reclamaciones
    
    def obtener_reclamos_dict(self):
        sql = """
            select 
                    id,
                    name,
                    consumer_document_type
                from bo_libro_reclamaciones 
                limit 2
        """
        request.env.cr.execute(sql)
        reclamaciones = request.env.cr.dictfetchall()
        # _logger.info(reclamaciones)
        return reclamaciones


    @http.route("/libro-reclamaciones",type="http",auth="public",methods=["GET"],website=True)
    def page_libro_reclamaciones(self,**kwargs):
        # return "<h1>Libro de reclamaciones website</h1>"
        _logger.info(kwargs)
        param = kwargs.get("param",'1')
        user = request.env.user
        if user.active:
            if user.has_group("bo_libro_reclamaciones.group_reclamacion_asistente"):
                company = request.env.company
                reclamos = self.obtener_reclamos_dict()
                reclamos_2 = self.obtener_reclamos_objs()
                return request.render("bo_libro_reclamaciones.page_lista_libro_reclamaciones",{"user":user,
                                                                                                "company":company,
                                                                                                "reclamos":reclamos,
                                                                                                "reclamos_2":reclamos_2,
                                                                                                "param":param})
            else:
                return "Usted no tiene los permisos para ingresar a este lista, contacte con su administrador"
        else:
            return request.redirect("/web/login")


    @http.route("/libro-reclamaciones",type="json",auth="user",methods=["POST"])
    def get_libro_reclamaciones(self):
        return self.obtener_reclamos_dict()

    @http.route("/libro-reclamaciones_v2",type="json",auth="user",methods=["POST"])
    def get_libro_reclamaciones_v2(self,limit=10,offset=0):
        reclamaciones = request.env["bo.libro.reclamaciones"].search([],limit=limit,offset=offset)
        return reclamaciones.read(fields=["name","state","consumer_name"])

    @http.route("/api/public/countrys",type="json",auth="public",methos=["POST"])
    def api_public_countrys(self):
        countrys = request.env["res.country"].sudo().search([])
        return countrys.read()
    
    @http.route("/api/reclamo/create",type="json",auth="public",methos=["POST"])
    def api_public_countrys(self,values):
        reclamo = request.env["bo.libro.reclamaciones"].sudo()
        reclamo_obj = reclamo.create(values)
        if reclamo_obj:
            return {"success":True}
        else:
            return {"success":False}