from odoo import fields,models


class WizardReportLR(models.TransientModel):
    _name = "wizard.reporte.lr"

    date_from = fields.Date("Fecha de Inicio",required=True)
    date_to = fields.Date("Fecha de Fin",required=True)

    def action_view_report(self):
        return {
            "type":"ir.actions.act_window",
            "view_mode":"tree",
            "res_model":"bo.libro.reclamaciones",
            "domain":[("date_order",">=",self.date_from),("date_order","<=",self.date_to)],
            "name":"Reporte de Reclamos desde {} hasta {}".format(self.date_from,self.date_to),
            "view_id":self.env.ref("bo_libro_reclamaciones.view_tree_libro_reclamaciones_responsable").id
        }