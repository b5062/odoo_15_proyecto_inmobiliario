from odoo import models, fields,api
from odoo.exceptions import UserError
import logging
log = logging.getLogger(__name__)
class LibroReclamaciones(models.Model):
    _name = "bo.libro.reclamaciones"
    _description = "Libro de reclamaciones"

    _sql_constraints = [('bo_name_unique','UNIQUE(name)','El número de reclamo debe ser único')]

    
    state = fields.Selection(string="Estado",selection=[('new','Nuevo'),
                                                        ('in_process','En proceso'),
                                                        ('cancel','Cancelado'),
                                                        ('resolved','Resuelto'),
                                                        ('retraso','Retraso en atención')],
                                            default="new")

    dias_retraso = fields.Integer("Tiempo de retraso (días)")

    # IDENTIFICACIÓN DEL CONSUMIDOR RECLAMANTE
    consumer_type = fields.Selection(selection=[('individual','Persona Natural'),
                                                ('company','Empresa')],
                                    string="Tipo de consumidor",
                                    default="individual")
    consumer_company_name = fields.Char(string="Razón Social")
    consumer_company_document = fields.Char(string="N° R.U.C.")

    consumer_name = fields.Char(string="Nombres")
    consumer_lastname = fields.Char(string="Apellidos")
    consumer_email = fields.Char(string="E-mail")
    consumer_document_type = fields.Selection(string="Tipo de documento de Identidad",
                                                selection=[('1','DNI'),('4','CE'),('7','Pasaporte')],
                                                default="1")
    consumer_document = fields.Char(string="Número de documento")
    consumer_phone = fields.Char(string="Teléfono")
    consumer_address = fields.Char(string="Dirección")
    
    consumer_country_id = fields.Many2one("res.country",
                                            default=lambda r:r.env.ref("base.pe",raise_if_not_found=False))
    consumer_state_id = fields.Many2one("res.country.state",string="Departamento")
    consumer_province_id = fields.Many2one("res.country.state",string="Provincia")
    consumer_district_id = fields.Many2one("res.country.state",string="Distrito")

    # DATOS DEL PADRE, MADRE O TUTOR
    consumer_younger = fields.Boolean(string="Es menor de edad?",default=False)
    consumer_younger_name = fields.Char(string="Nombres")
    consumer_younger_lastname = fields.Char(string="Apellidos")
    consumer_younger_document = fields.Char(string="DNI/CE")

    # IDENTIFICACIÓN DEL BIEN CONTRATADO
    product_type = fields.Selection(string="Tipo de producto",selection=[('product','Producto'),('service','Servicio')],default="product")
    product_code = fields.Char(string="Código de producto")
    order_name = fields.Char(string="Número de órden de venta")
    date_order = fields.Date(string="Fecha de venta")
    product_name = fields.Char(string="Nombre de producto")

    # DETALLE DE RECLAMO O QUEJA
    claim_type = fields.Selection(string="Tipo de reclamación",selection=[('reclamo','Reclamo'),('queja','Queja')],default="reclamo")
    claim_amount = fields.Float(string="Monto reclamado")
    claim_detail = fields.Text(string="Detalle de reclamo")
    claim_request = fields.Text(string="Solicitud de reclamo")

    company_id = fields.Many2one("res.company",default=lambda self:self.env.company)
    currency_id = fields.Many2one("res.currency",default=lambda self:self.env.company.currency_id.id)
    
    name = fields.Char("Número de Reclamo",default="/")
    # claim_user_id = fields.Many2one("res.users",default=lambda self:self.env.company.default_claim_user_id.id)
    
    

    def action_in_process(self):
        if self.env.user.has_group('bo_libro_reclamaciones.group_reclamacion_responsable'):
            if self.state == "new":
                self.state = "in_process"
            else:
                raise UserError("El estado debe encontrarse en nuevo para poder procesar el reclamo")
        else:
            raise UserError("Alerta!!! No tienes permitido procesar este reclamo")


    def action_cancel(self):
        if self.state == "in_process":
            self.state = "cancel"
        else:
            raise UserError("El estado debe encontrarse en proceso para poder cencelarse")

    def action_ok(self):
        if self.state == "in_process":
            self.state = "resolved"
        else:
            raise UserError("El estado debe encontrarse en proceso para pasar a resuelto")

    def action_open(self):
        return {
            "type":"ir.actions.act_window",
            "name":"Libro de reclamaciones",
            "view_mode":"form",
            "res_model":"bo.libro.reclamaciones",
            "res_id":self.id,
            "target":"new"
        }

    @api.model
    def create(self, values):
        # sequence_lr = self.env.ref("bo_libro_reclamaciones.sequence_libro_reclamaciones")
        sequence_lr = self.env.company.claim_sequence_id
        name = sequence_lr.next_by_id()
        values.update({
            "name":name
        })
        log.info(values)
        return super(LibroReclamaciones, self).create(values)

    @api.model
    def cron_actualizar_retraso_de_atencion(self):
        reclamos = self.search([])
        for reclamo in reclamos:
            if reclamo.state == 'new':
                now = fields.Date.today()
                log.info(now)
                log.info(reclamo.create_date.date())
                log.info((now - reclamo.create_date.date()).days)
                reclamo.dias_retraso = (now - reclamo.create_date.date()).days
                if reclamo.dias_retraso > 5:
                    reclamo.state = 'retraso'