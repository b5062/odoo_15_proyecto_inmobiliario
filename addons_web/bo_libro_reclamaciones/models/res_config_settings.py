from odoo import fields,models


class ResCompany(models.Model):
    _inherit = "res.company"

    #Campo de secuencia de reclamo por defecto para los reclamos de un a compañia
    claim_sequence_id = fields.Many2one("ir.sequence",string="Secuencia por defecto para LR")


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    claim_sequence_id = fields.Many2one("ir.sequence",related='company_id.claim_sequence_id',readonly=False)