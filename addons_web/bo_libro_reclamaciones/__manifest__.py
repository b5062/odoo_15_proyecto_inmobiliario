{
    "name":"Libro de reclamaciones web",
    "author":"Daniel de BigOdoo",
    "description":"""
        Este módulo le permitirá gestionar los reclamos y quejas de sus clientes
    """,
    "depends":[
        "base","base_setup","sale"
    ],
    "data":[
        "demo/demo.xml",
        "security/groups.xml",
        "security/ir.model.access.csv",
        "data/ir_sequence.xml",
        "data/ir_cron.xml",
        "views/res_config_settings.xml",
        "views/libro_reclamaciones_asistente.xml",
        "views/libro_reclamaciones_responsable.xml",
        "views/wizard_report_lr.xml",
        "views/menu.xml",
        "templates/lista_libro_reclamaciones.xml"
    ]
}