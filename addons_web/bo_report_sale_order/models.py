from odoo import models,api,fields,tools




class BOReportSaleOrder(models.Model):
    _name = "bo.report.sale.order"
    _auto = False
    # _rec_name = "cliente"

    name = fields.Char("Nombre")
    order_id = fields.Many2one("sale.order")
    cliente = fields.Many2one("res.partner")
    vendedor = fields.Many2one("res.users")
    product_id = fields.Many2one("product.product")
    price_unit = fields.Float("PU")
    subtotal = fields.Float("Subtotal")
    total = fields.Float("Total")
    porcentaje_descuento = fields.Float("% Descuento")
    monto_descuento = fields.Float("Monto Descuento")
    date_order = fields.Date("Fecha de venta")
    company_id = fields.Many2one("res.company")
    categoria = fields.Many2one("product.category")
    def init(self):
        sql = """
            CREATE OR REPLACE VIEW {} AS (
                select sol.id,
                    so.name,
                    sol.order_id,
                    so.partner_id as cliente,
                    so.user_id as vendedor,
                    sol.product_id,
                    sol.price_unit,
                    sol.price_subtotal as subtotal,
                    sol.price_total as total,
                    sol.discount as porcentaje_descuento,
                    (sol.discount*sol.price_total/100) as monto_descuento,
                    so.date_order,
                    so.company_id,
                    pc.id as categoria
                from sale_order so
                left join sale_order_line sol on sol.order_id = so.id
                left join product_product pp on pp.id = sol.product_id 
                left join product_template pt on pt.id = pp.product_tmpl_id 
                left join product_category pc on pc.id = pt.categ_id 
            )
        """.format(self._table)

        tools.drop_view_if_exists(self.env.cr,self._table)
        self.env.cr.execute(sql)