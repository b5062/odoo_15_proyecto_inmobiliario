{
    "name":"Reporte de ventas",
    "depends":[
        "base","base_setup","sale"
    ],
    "data":[
        "ir.model.access.csv",
        "ir_rule.xml",
        "views.xml"
    ]
}